from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(

    veilcoin=math.Object(
        PARENT=networks.nets['veilcoin'],
        SHARE_PERIOD=15, # seconds
        CHAIN_LENGTH=15*60*60//15, # shares
        REAL_CHAIN_LENGTH=15*60*60//15, # shares
        TARGET_LOOKBEHIND=60, # shares
        SPREAD=8, # blocks
        IDENTIFIER='b63d0305612b598b'.decode('hex'),
        PREFIX='5ed924ee4bb4303c'.decode('hex'),
        P2P_PORT=13693,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=13692,
        BOOTSTRAP_ADDRS='veil.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-veil',
        VERSION_CHECK=lambda v: True,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
