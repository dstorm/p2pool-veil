import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(

    veilcoin=math.Object(
        P2P_PREFIX='a1b9aa7f'.decode('hex'),
        P2P_PORT=14693,
        ADDRESS_VERSION=70,
        RPC_PORT=4694,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'veilcoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda height: 60*100000000 >> (height + 1)//20000,
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('x13_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('x13_hash').getPoWHash(data)),
        BLOCK_PERIOD=240, # s
        SYMBOL='VEIL',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'VeilCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/VeilCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.veilcoin'), 'veilcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
    ),

)
for net_name, net in nets.iteritems():
    net.NAME = net_name
